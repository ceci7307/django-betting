from django.shortcuts import render

def test(request):
    context = {}
    return render( request, 'betting_app/test.html', context)

def bettingAdmin(request):
    context = {}
    return render( request, 'betting_app/betting-admin.html', context)

def createBet(request):
    context = {}
    return render( request, 'betting_app/admin-create-bet.html', context)

def singleview(request):
    context = {}
    return render( request, 'betting_app/customer-singleview.html', context)

