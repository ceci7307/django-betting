from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('test/', views.test, name='test'),
    path('betting-admin/', views.bettingAdmin, name='bettingAdmin'),
    path('create-bet/', views.createBet, name='createBet'),
    path('singleview/', views.singleview, name='singleview'),
]